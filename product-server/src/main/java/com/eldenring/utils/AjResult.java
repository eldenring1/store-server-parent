package com.eldenring.utils;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AjResult {

  private Boolean success = true;

  private String message = "添加成功";

}
