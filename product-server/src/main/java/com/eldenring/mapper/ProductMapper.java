package com.eldenring.mapper;


import com.eldenring.domain.Product;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface ProductMapper {

    void save(Product product);

}
