package com.eldenring.controller;


import com.eldengring.domain.Store;
import com.eldenring.domain.Product;
import com.eldenring.feign.StoreFeignClient;
import com.eldenring.server.IProductServer;
import com.eldenring.utils.AjResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ProductController {

    @Autowired
    private IProductServer productServer;

    @Autowired
    private StoreFeignClient storeFeignClient;


    @PostMapping("/productSave")
    public AjResult productSave(@RequestBody Product product){
        productServer.save(product);
        Store store = new Store();
        store.setProduct_id(product.getId());
        store.setNumber(product.getNumber());
        return storeFeignClient.saveStore(store);
    }


}
