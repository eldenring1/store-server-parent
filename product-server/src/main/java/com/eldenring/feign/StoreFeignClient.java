package com.eldenring.feign;

import com.eldengring.domain.Store;
import com.eldenring.utils.AjResult;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;

@FeignClient(name = "store-server", fallbackFactory = StoreFeignClientFallbackFactory.class)
public interface StoreFeignClient {

    @PostMapping("/saveStore")
    AjResult saveStore(Store store);

}
