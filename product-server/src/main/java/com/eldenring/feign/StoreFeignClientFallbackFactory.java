package com.eldenring.feign;


import com.eldengring.domain.Store;
import com.eldenring.utils.AjResult;
import feign.hystrix.FallbackFactory;
import org.springframework.stereotype.Component;

@Component
public class StoreFeignClientFallbackFactory implements FallbackFactory<StoreFeignClient> {
    @Override
    public StoreFeignClient create(Throwable throwable) {
        return new StoreFeignClient() {
            @Override
            public AjResult saveStore(Store store) {
                return new AjResult(false,"我是降级方法");
            }
        };
    }
}
