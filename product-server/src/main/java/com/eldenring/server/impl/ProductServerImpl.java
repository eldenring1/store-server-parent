package com.eldenring.server.impl;

import com.eldenring.domain.Product;
import com.eldenring.mapper.ProductMapper;
import com.eldenring.server.IProductServer;
import org.springframework.beans.factory.annotation.Autowired;

public class ProductServerImpl implements IProductServer {

    @Autowired
    private ProductMapper productMapper;

    @Override
    public void save(Product product) {
        productMapper.save(product);
    }
}
