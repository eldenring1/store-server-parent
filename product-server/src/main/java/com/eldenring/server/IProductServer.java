package com.eldenring.server;

import com.eldenring.domain.Product;
import com.eldenring.mapper.ProductMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

public interface IProductServer {
    void save(Product product);

    @Service
    class ProductServerImpl implements IProductServer {

        @Autowired
        private ProductMapper productMapper;

        @Override
        public void save(Product product) {
            productMapper.save(product);
        }
    }
}
