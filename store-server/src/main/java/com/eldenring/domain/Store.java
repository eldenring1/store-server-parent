package com.eldenring.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Negative;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Store {

    private Long id;

    private Long product_id;

    private Long number;

}
