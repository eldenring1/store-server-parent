package com.eldenring.controller;

import com.eldenring.domain.Store;
import com.eldenring.server.IStoreServer;
import com.eldenring.utils.AjResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class StoreController {

    @Autowired
    private IStoreServer storeServer;


    @PostMapping("/saveStore")
    public AjResult saveStore(@RequestBody Store store){
        storeServer.save(store);
        return new AjResult();
    }


}
