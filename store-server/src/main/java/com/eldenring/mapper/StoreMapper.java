package com.eldenring.mapper;


import com.eldenring.domain.Store;

public interface StoreMapper {

    void save(Store store);

}
