package com.eldenring.server;


import com.eldenring.domain.Store;

public interface IStoreServer {

    void save(Store store);
}
