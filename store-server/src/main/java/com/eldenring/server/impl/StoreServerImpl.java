package com.eldenring.server.impl;

import com.eldenring.domain.Store;
import com.eldenring.mapper.StoreMapper;
import com.eldenring.server.IStoreServer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class StoreServerImpl implements IStoreServer {

    @Autowired
    private StoreMapper storeMapper;

    @Override
    public void save(Store store) {
        storeMapper.save(store);
    }
}
