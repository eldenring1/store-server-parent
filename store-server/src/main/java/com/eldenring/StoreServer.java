package com.eldenring;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@SpringBootApplication
@EnableEurekaClient
@MapperScan("com.eldenring.mapper")
public class StoreServer {

    public static void main(String[] args) {
        SpringApplication.run(StoreServer.class,args);
    }
}
